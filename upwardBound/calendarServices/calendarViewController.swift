//
//  calendarViewController.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/14/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import FSCalendar
import Parse

class calendarViewController: UIViewController,UITableViewDataSource, UITableViewDelegate, FSCalendarDataSource, FSCalendarDelegate   {
    var isAdmin = ""
     fileprivate let gregorian = Calendar(identifier: .gregorian)
    @IBOutlet weak var calendar: FSCalendar!
    var name = ""
     var dataParse:NSMutableArray = NSMutableArray()
     var eventsParse:NSMutableArray = NSMutableArray()

    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        return formatter
    }()
    
    @IBOutlet weak var tableView: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.dataParse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for:indexPath) as UITableViewCell
        let cellDataParse:PFObject = self.dataParse.object(at: indexPath.row) as! PFObject
        cell.textLabel?.text = cellDataParse.object(forKey: "eventTitle") as? String
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let currentUser = PFUser.current()
        name = (currentUser?.username)!
        
        }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition){
               self.dataParse.removeAllObjects()
        let selectedDates = self.dateFormatter.string(from: date)
               let query = PFQuery(className:"classEvent")
               query.whereKey("Date", equalTo:selectedDates)
       
               query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
                   if error == nil {
       
                       for object:PFObject! in objects! {
                           self.dataParse.add(object)
                       }
                    
                       self.tableView.reloadData()
                  
                   }
               })
    
        
    }
    
    func fetchData(date: String){
        let query = PFQuery(className:"classEvent")
        query.whereKey("Date", equalTo:date)
        
        
                query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
                    if error == nil {
        
                        for object:PFObject! in objects! {
                            self.eventsParse.add(object)
                           
                            }
                    }
            })
    
    }
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        var numEvents = 0
        let currentDate = self.dateFormatter.string(from: date)
        fetchData(date: currentDate)
    //    for objects in eventsParse{
            //let dateObj:PFObject = self.eventsParse.object(at: objects as! Int) as! PFObject
          //  if (objects["Date"] as! String == currentDate)
          //  {
              //  numEvents = numEvents + 1
           // }
      //  }
        //cell.textLabel?.text = cellDataParse.object(forKey: "eventTitle") as? String
        
        return numEvents;
    }

    @IBAction func homeButton(_ sender: Any) {
        let currentUser = PFUser.current()
        self.isAdmin = (currentUser!["isAdmin"])! as! String
        if (self.isAdmin == "1")
        {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "adminLandingPage") as! adminLandingPageViewController
            self.present(vc, animated: true, completion: nil)
        }
        else {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "landingPage") as! landingPageViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func csButton(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "csMain") as! communityServiceViewController
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func messageButton(_ sender: Any) {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "messageMain") as! chatsViewController
        let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
        self.present(navController, animated:true, completion: nil)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
