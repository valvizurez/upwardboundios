//
//  signUpViewController.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/17/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Parse

class signUpViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var userName: UITextField!
    @IBOutlet var userPassword: UITextField!
    @IBOutlet var userFirstName: UITextField!
    @IBOutlet var userLastName: UITextField!
    @IBAction func signUpButton(_ sender: Any) {
        
        var user = PFUser()
        user.username = userName.text
        user.password = userPassword.text
        user["firstName"] = userFirstName.text
        user["lastName"] = userLastName.text
       // user["isAdmin"] = "0"

        // other fields can be set just like with
        
        user.signUpInBackground(block: { (succeed, error) -> Void in
            if ((error) != nil) {
                var alert = UIAlertView(title: "Error", message: "\(error)", delegate: self, cancelButtonTitle: "OK")
                alert.show()
            }
            else {
                var alert = UIAlertView(title: "Success", message: "Signed Up", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                DispatchQueue.global().async {
                    DispatchQueue.main.async {
                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "landingPage") as! landingPageViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userName.delegate = self
        self.userPassword.delegate = self      // Do any additional setup after loading the view.
        self.userFirstName.delegate = self
        self.userLastName.delegate = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        userName.resignFirstResponder()
        userPassword.resignFirstResponder()
        userFirstName.resignFirstResponder()
        userLastName.resignFirstResponder()
        return true
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
