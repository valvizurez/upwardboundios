//
//  addCommunityServiceViewController.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/18/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Parse

class addCommunityServiceViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var userDate: UITextField!
    @IBOutlet var userHours: UITextField!
    @IBOutlet var eventName: UITextField!
      var name  = ""
    
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userDate.delegate = self as? UITextFieldDelegate
         showDatePicker()
        let currentUser = PFUser.current()
        name = (currentUser?.username)!
        self.userHours.delegate = self
        self.eventName.delegate = self
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func saveCommunityService(_ sender: Any) {
        var newHours = PFObject(className:"userHours")
        newHours["eventName"] = eventName.text
        newHours["userHours"] = userHours.text
        newHours["Date"] = userDate.text
        newHours["userId"] = name
        newHours["verified"] = 0
        newHours.saveInBackground {
            (success: Bool, error: Error?) in
            if (success) {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "csMain") as! communityServiceViewController
                self.present(vc, animated: true, completion: nil)
            } else {
                let alert = UIAlertView(title: "Error", message: "\(String(describing: error))", delegate: self, cancelButtonTitle: "OK")
                alert.show()
            }
            
        }
    }
    

    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.bordered, target: self,  action: #selector(addCommunityServiceViewController.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.bordered, target: self, action:#selector(addCommunityServiceViewController.cancelDatePicker))
        
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        // add toolbar to textField
        userDate.inputAccessoryView = toolbar
        // add datepicker to textField
        userDate.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        userDate.text = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        eventName.resignFirstResponder()
        userHours.resignFirstResponder()
        return true
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
