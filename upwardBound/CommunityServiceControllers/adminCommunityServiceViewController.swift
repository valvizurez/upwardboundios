//
//  adminCommunityServiceViewController.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/18/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import ScrollableGraphView
import Parse

class adminCommunityServiceViewController: UIViewController, ScrollableGraphViewDataSource, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var tableView: UITableView!
    
    var dataParse:NSMutableArray = NSMutableArray()
    var name = ""
    var valueToPass = ""
    var graphView: ScrollableGraphView!
    var jan = 0
    var feb = 0
    var march = 0
    var apr = 0
    var may = 0
    var jun = 0
    var jul = 0
    var aug = 0
    var sep = 0
    var oct = 0
    var nov = 0
    var dec = 0
    lazy var xAxisLabels: [String] = ["Jan", "Feb", "March", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Nov","Dec"]
    lazy var values: [Int] = [jan,feb,march,apr,may,jun, jul, aug, sep, nov, dec]
    @IBOutlet var barChart: UIView!
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataParse.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for:indexPath) as UITableViewCell
        let cellDataParse:PFObject = self.dataParse.object(at: indexPath.row) as! PFObject
        cell.textLabel?.text = cellDataParse.object(forKey: "eventName") as? String
        cell.detailTextLabel?.text = cellDataParse.object(forKey: "Date") as? String
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow;
        _ = tableView.cellForRow(at: indexPath!) as UITableViewCell!;
        let event:PFObject = self.dataParse.object(at: indexPath!.row) as! PFObject
        valueToPass = (event.object(forKey: "eventName") as? String)!
        self.performSegue(withIdentifier: "showCSevent", sender: self)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.dataParse.removeAllObjects()
        fetchData()
       // updateValues()
    }
    
    func fetchData(){
        let query = PFQuery(className:"userHours")
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            if error == nil {
                
                for object:PFObject! in objects! {
                    self.dataParse.add(object)
                }
                self.tableView.reloadData()
            }
        })
    }
    
    
    func value(forPlot plot: Plot, atIndex pointIndex: Int) -> Double {
        return Double(values[pointIndex])
    }
    
    func label(atIndex pointIndex: Int) -> String {
        return xAxisLabels[pointIndex]
    }
    
    func numberOfPoints() -> Int {
        return 11
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let barCord: CGRect = barChart.frame
        let frame = CGRect(x: barCord.origin.x , y: barCord.origin.y, width: barCord.size.width, height: barCord.size.height)
        
        graphView = createBarGraph(frame)
        barChart.addSubview(graphView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func createBarGraph(_ frame: CGRect) -> ScrollableGraphView {
        
        
        // Do any additional setup after loading the view.
        let graphView = ScrollableGraphView(frame: frame, dataSource: self as ScrollableGraphViewDataSource)
        
        // Setup the plot
        let barPlot = BarPlot(identifier: "bar")
        
        barPlot.barWidth = 25
        barPlot.barLineWidth = 1
        barPlot.barLineColor = UIColor.lightGray
        barPlot.barColor = UIColor.darkGray
        
        barPlot.adaptAnimationType = ScrollableGraphViewAnimationType.elastic
        barPlot.animationDuration = 1.5
        
        // Setup the reference lines
        let referenceLines = ReferenceLines()
        
        referenceLines.referenceLineLabelFont = UIFont.boldSystemFont(ofSize: 8)
        referenceLines.referenceLineColor = UIColor.white.withAlphaComponent(0.2)
        referenceLines.referenceLineLabelColor = UIColor.white
        
        referenceLines.dataPointLabelColor = UIColor.white.withAlphaComponent(0.5)
        
        // Setup the graph
        graphView.backgroundFillColor = UIColor.black
        
        graphView.shouldAnimateOnStartup = true
        
        graphView.rangeMax = 100
        graphView.rangeMin = 0
        
        // Add everything
        graphView.addPlot(plot: barPlot)
        graphView.addReferenceLines(referenceLines: referenceLines)
        return graphView
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
