//
//  communityServiceViewController.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/14/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import ScrollableGraphView
import Parse

class communityServiceViewController: UIViewController, ScrollableGraphViewDataSource, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var studentNamePicker: UITextField!
    @IBOutlet var tableView: UITableView!
    let salutations = ["", "Mr.", "Ms.", "Mrs."]
    var isAdmin = ""
    var dataParse:NSMutableArray = NSMutableArray()
    var name = ""
    var valueToPass = ""
    var graphView: ScrollableGraphView!
    var jan = 0
    var feb = 0
    var march = 0
    var apr = 0
    var may = 0
    var jun = 0
    var jul = 0
    var aug = 0
    var sep = 0
    var oct = 0
    var nov = 0
    var dec = 0
    lazy var xAxisLabels: [String] = ["Jan", "Feb", "March", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Nov","Dec"]
    lazy var values: [Int] = [jan,feb,march,apr,may,jun, jul, aug, sep, nov, dec]
    @IBOutlet var barChart: UIView!
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataParse.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for:indexPath) as UITableViewCell
        let cellDataParse:PFObject = self.dataParse.object(at: indexPath.row) as! PFObject
        cell.textLabel?.text = cellDataParse.object(forKey: "eventName") as? String
        cell.detailTextLabel?.text = cellDataParse.object(forKey: "Date") as? String
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow;
        _ = tableView.cellForRow(at: indexPath!) as UITableViewCell!;
        let event:PFObject = self.dataParse.object(at: indexPath!.row) as! PFObject
        valueToPass = (event.object(forKey: "eventName") as? String)!
        self.performSegue(withIdentifier: "showCSevent", sender: self)
    }
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showCSevent") {
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destination as! showCommunityServiceViewController
            // your new view controller should have property that will store passed value
            viewController.passedValue = valueToPass
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.dataParse.removeAllObjects()
        fetchData()
        updateValues()
        
    }
    
    func fetchData(){
       let query = PFQuery(className:"userHours")
        if (self.isAdmin == "0")
        {
            query.whereKey("userId", equalTo:name)
        }
        
        //query.whereKey("userId", equalTo:name)
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            if error == nil {
                
                for object:PFObject! in objects! {
                    self.dataParse.add(object)
                }
                self.tableView.reloadData()
            }
        })
    }
    
    
    func value(forPlot plot: Plot, atIndex pointIndex: Int) -> Double {
        return Double(values[pointIndex])
    }
    
    func label(atIndex pointIndex: Int) -> String {
         return xAxisLabels[pointIndex]
    }
    
    func numberOfPoints() -> Int {
        return 11
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let currentUser = PFUser.current()
        name = (currentUser?.username)!
       let barCord: CGRect = barChart.frame
        let frame = CGRect(x: barCord.origin.x , y: barCord.origin.y, width: barCord.size.width, height: barCord.size.height)
         self.isAdmin = (currentUser!["isAdmin"])! as! String
         graphView = createBarGraph(frame)
        barChart.addSubview(graphView)
        
    }

    func updateValues(){
        var tempHour = ""
        var tempDate = ""
        let dateForm = DateFormatter()
        dateForm.dateFormat = "MM/dd/yyyy"
        let monthForm = DateFormatter()
        monthForm.dateFormat = "MMMM"
        let query = PFQuery(className:"userHours")
        if (self.isAdmin == "0"){
            query.whereKey("userId", equalTo:name)
        }
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            if error == nil {
                
                for userHours:PFObject! in objects!
                {
                    tempHour = (userHours.object(forKey:"userHours") as? String)!
                    tempDate = (userHours.object(forKey:"Date") as? String)!
                    let date = dateForm.date(from: tempDate)
                    let month = monthForm.string(from: date!)
                    
                    switch month
                    {
                    case "January":
                        self.jan = self.jan + Int(tempHour)!
                    case "February":
                        self.feb = self.feb + Int(tempHour)!
                    case "March":
                        self.march = self.march + Int(tempHour)!
                    case "April":
                        self.apr = self.apr + Int(tempHour)!
                    case "May":
                        self.may = self.may + Int(tempHour)!
                    case "June":
                        self.jun = self.jun + Int(tempHour)!
                    case "July":
                        self.jul = self.jul + Int(tempHour)!
                    case "August":
                        self.aug = self.aug + Int(tempHour)!
                    case "September":
                        self.sep = self.sep + Int(tempHour)!
                    case "October":
                        self.oct = self.oct + Int(tempHour)!
                    case "November":
                        self.nov = self.nov + Int(tempHour)!
                    case "December":
                        self.dec = self.dec + Int(tempHour)!
                    default:
                        0
                    }
                    self.values = [self.jan,self.feb,self.march,self.apr,self.may,self.jun, self.jul, self.aug, self.sep, self.nov, self.dec]
                    self.graphView.reload()
                }
               
            }
        })
        
        
      
    }
 private func createBarGraph(_ frame: CGRect) -> ScrollableGraphView {
    
    
    // Do any additional setup after loading the view.
    let graphView = ScrollableGraphView(frame: frame, dataSource: self as ScrollableGraphViewDataSource)
    
    // Setup the plot
    let barPlot = BarPlot(identifier: "bar")
    
    barPlot.barWidth = 25
    barPlot.barLineWidth = 1
    barPlot.barLineColor = UIColor.lightGray
    barPlot.barColor = UIColor.darkGray
    
    barPlot.adaptAnimationType = ScrollableGraphViewAnimationType.elastic
    barPlot.animationDuration = 1.5
    
    // Setup the reference lines
    let referenceLines = ReferenceLines()
    
    referenceLines.referenceLineLabelFont = UIFont.boldSystemFont(ofSize: 8)
    referenceLines.referenceLineColor = UIColor.white.withAlphaComponent(0.2)
    referenceLines.referenceLineLabelColor = UIColor.white
    
    referenceLines.dataPointLabelColor = UIColor.white.withAlphaComponent(0.5)
    
    // Setup the graph
    graphView.backgroundFillColor = UIColor.black
    
    graphView.shouldAnimateOnStartup = true
    
    graphView.rangeMax = 100
    graphView.rangeMin = 0
    
    // Add everything
    graphView.addPlot(plot: barPlot)
    graphView.addReferenceLines(referenceLines: referenceLines)
    return graphView
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func homeButton(_ sender: Any) {
        let currentUser = PFUser.current()
        self.isAdmin = (currentUser!["isAdmin"])! as! String
        if (self.isAdmin == "1")
        {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "adminLandingPage") as! adminLandingPageViewController
            self.present(vc, animated: true, completion: nil)
        }
        else {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "landingPage") as! landingPageViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func calendarButton(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "calendarMain") as! calendarViewController
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func messagingButton(_ sender: Any) {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "messageMain") as! chatsViewController
        let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
        self.present(navController, animated:true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
