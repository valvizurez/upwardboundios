//
//  showCommunityServiceViewController.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/18/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Parse

class showCommunityServiceViewController: UIViewController {
    @IBOutlet var userEventName: UILabel!
    @IBOutlet var userHours: UILabel!
    @IBOutlet var userDate: UILabel!
    var eventID = ""
    var passedValue = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventID = passedValue
        fetchData()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchData(){
        let query = PFQuery(className:"userHours")
        query.whereKey("eventName", equalTo:eventID)
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            if error == nil {
                
                for event:PFObject! in objects! {
                    self.userEventName.text = (event.object(forKey:"eventName") as? String)!
                    self.userHours.text = (event.object(forKey:"userHours") as? String)!
                    self.userDate.text = (event.object(forKey:"Date") as? String)!
                }
               
            }
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
