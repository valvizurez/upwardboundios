//
//  loginViewController.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/17/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import FacebookLogin
import ParseFacebookUtils
import FacebookCore
import Parse



class loginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var userName: UITextField!
    @IBOutlet var userPassword: UITextField!
    var isAdmin = ""
    let permissionsArray = ["public_profile", "email"];
    
    @IBAction func userLogin(_ sender: Any) {
        PFUser.logInWithUsername(inBackground: userName.text!, password: userPassword.text!, block: { (user, error) -> Void in
            
            if ((user) != nil) {
                var alert = UIAlertView(title: "Success", message: "Logged In", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
                DispatchQueue.global().async {
                    DispatchQueue.main.async {
                        let currentUser = PFUser.current()
                        self.isAdmin = (currentUser!["isAdmin"])! as! String
                        if (self.isAdmin == "1")
                        {
                            let vc = self.storyboard!.instantiateViewController(withIdentifier: "adminLandingPage") as! adminLandingPageViewController
                            self.present(vc, animated: true, completion: nil)
                        }
                        else {
                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "landingPage") as! landingPageViewController
                        self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
                
            }
            else {
                var alert = UIAlertView(title: "Error", message: "\(error)", delegate: self, cancelButtonTitle: "OK")
                alert.show()
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userName.delegate = self
        self.userPassword.delegate = self
 
    }
   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        userName.resignFirstResponder()
        userPassword.resignFirstResponder()
        return true
        
    }
    @IBAction func loginWithFacebook(_ sender: Any) {
        PFFacebookUtils.initializeFacebook()
        
        PFFacebookUtils.logIn(withPermissions: permissionsArray, block: {
            (user: PFUser!, error: NSError!) -> Void in
            if user != nil {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "landingPage") as! landingPageViewController
                self.present(vc, animated: true, completion: nil)
            } else if error != nil{
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "landingPage") as! landingPageViewController
                self.present(vc, animated: true, completion: nil)
            } else {
                print(error)
            }
            
            } as! PFUserResultBlock )
    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
