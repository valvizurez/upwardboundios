//
//  Constants.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/23/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import Foundation
import Firebase

struct Constants
{
    struct refs
    {
        static let databaseRoot = Database.database().reference()
        static let databaseChats = databaseRoot.child("chats")
    }
}
