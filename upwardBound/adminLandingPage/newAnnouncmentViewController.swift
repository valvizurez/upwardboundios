//
//  newAnnouncmentViewController.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/26/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Parse

class newAnnouncmentViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var announceTitle: UITextField!
    @IBOutlet weak var date: UITextField!
    @IBOutlet weak var announceDetail: UITextView!
    
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        date.delegate = self as? UITextFieldDelegate
        showDatePicker()
        
        self.announceTitle.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveNewAnnounce(_ sender: Any) {
        var newAnnounce = PFObject(className:"classAnnoucement")
        newAnnounce["announceTitle"] = announceTitle.text
        newAnnounce["Date"] = date.text
        newAnnounce["announceDetail"] = announceDetail.text
        newAnnounce.saveInBackground {
            (success: Bool, error: Error?) in
            if (success) {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "adminLandingPage") as! adminLandingPageViewController
                self.present(vc, animated: true, completion: nil)
            } else {
                let alert = UIAlertView(title: "Error", message: "\(String(describing: error))", delegate: self, cancelButtonTitle: "OK")
                alert.show()
            }
            
        }
    }
    
   
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.bordered, target: self,  action: #selector(newAnnouncmentViewController.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.bordered, target: self, action:#selector(newAnnouncmentViewController.cancelDatePicker))
        
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        // add toolbar to textField
        date.inputAccessoryView = toolbar
        // add datepicker to textField
        date.inputView = datePicker
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        announceTitle.resignFirstResponder()
        announceDetail.resignFirstResponder()
        return true
        
    }
    
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        date.text = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    


}
