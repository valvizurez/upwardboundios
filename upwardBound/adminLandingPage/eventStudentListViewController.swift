
//
//  eventStudentListViewController.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/26/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Parse
class eventStudentListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var dataParse:NSMutableArray = NSMutableArray()
    var eventName = ""
    var passedValue = ""
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return self.dataParse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentCell", for:indexPath) as UITableViewCell
        let cellDataParse:PFObject = self.dataParse.object(at: indexPath.row) as! PFObject
        let firstName = cellDataParse.object(forKey: "studentfName") as? String
        let lastName = cellDataParse.object(forKey: "studentlName") as? String
        cell.textLabel?.text = lastName! + ", " + firstName!
      
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        eventName = passedValue
           fetchData()
       eventNameLabel.text = eventName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func fetchData(){
        let query = PFQuery(className:"studentEvents")
        query.whereKey("eventName", equalTo:eventName)
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            if error == nil {
                
                for object:PFObject! in objects! {
                    self.dataParse.add(object)
                }
                self.tableView.reloadData()
            }
        })
    }
    

}
