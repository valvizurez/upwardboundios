//
//  Channel.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/23/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

internal class Channel {
    internal let id: String
    internal let name: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}
