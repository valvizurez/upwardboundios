//
//  chatsViewController.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/23/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Firebase
import Parse

enum Section: Int {
    case createNewChannelSection = 0
    case currentChannelsSection
}

class chatsViewController: UIViewController,  UITableViewDataSource, UITableViewDelegate {
    
    var userName = ""
    var isAdmin = ""
    @IBOutlet var tableView: UITableView!
    var newChannelTextField: UITextField?
    var senderDisplayName: String?
    private var channels: [Channel] = []
    private lazy var channelRef: DatabaseReference = Database.database().reference().child("channels")
    private var channelRefHandle: DatabaseHandle?
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2 // 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let currentSection: Section = Section(rawValue: section){
            switch currentSection{
            case .createNewChannelSection:
                return 1
            case .currentChannelsSection:
                return channels.count
            }
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier = (indexPath as NSIndexPath).section == Section.createNewChannelSection.rawValue ? "NewChannel" : "ExistingChannel"
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        
        if (indexPath as NSIndexPath).section == Section.createNewChannelSection.rawValue {
            if let createNewChannelCell = cell as? CreateChannelCell {
                newChannelTextField = createNewChannelCell.newChannelNameField
            }
        } else if (indexPath as NSIndexPath).section == Section.currentChannelsSection.rawValue {
            cell.textLabel?.text = channels[(indexPath as NSIndexPath).row].name
        }
        
        return cell
    }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == Section.currentChannelsSection.rawValue {
            let channel = channels[(indexPath as NSIndexPath).row]
            self.performSegue(withIdentifier: "ShowChannel", sender: channel)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let channel = sender as? Channel {
            let chatVc = segue.destination as! messagingViewController
            
            chatVc.senderDisplayName = userName
            chatVc.channel = channel
            chatVc.channelRef = channelRef.child(channel.id)
        }
    }
    private func observeChannels() {
        // Use the observe method to listen for new
        // channels being written to the Firebase DB
        channelRefHandle = channelRef.observe(.childAdded, with: { (snapshot) -> Void in // 1
            let channelData = snapshot.value as! Dictionary<String, AnyObject> // 2
            let id = snapshot.key
            if let name = channelData["name"] as! String!, name.characters.count > 0 { // 3
                self.channels.append(Channel(id: id, name: name))
                self.tableView.reloadData()
            } else {
                print("Error! Could not decode channel data")
            }
        })
    }

//    @IBAction func createChannel(_ sender: AnyObject) {
//        if let name = newChannelTextField?.text { // 1
//            let newChannelRef = channelRef.childByAutoId() // 2
//            let channelItem = [ // 3
//                "name": name
//            ]
//            newChannelRef.setValue(channelItem) // 4
//        }
//    }
    @IBAction func createChannel(_ sender: Any) {
              if let name = newChannelTextField?.text { // 1
                  let newChannelRef =
        channelRef.childByAutoId() // 2
                  let channelItem = [ // 3
                      "name": name
                  ]
                  newChannelRef.setValue(channelItem) // 4
              } 
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Upward Bound Chat"
        observeChannels()
        let currentUser = PFUser.current()
        userName = (currentUser?.username)!
        self.senderDisplayName = userName
        // Do any additional setup after loading the view.
    }
    
    deinit {
        if let refHandle = channelRefHandle {
            channelRef.removeObserver(withHandle: refHandle)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   

    @IBAction func homeButton(_ sender: Any) {
        let currentUser = PFUser.current()
        self.isAdmin = (currentUser!["isAdmin"])! as! String
        if (self.isAdmin == "1")
        {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "adminLandingPage") as! adminLandingPageViewController
            self.present(vc, animated: true, completion: nil)
        }
        else {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "landingPage") as! landingPageViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
//    @IBAction func calendarButton(_ sender: Any) {
//        let vc = self.storyboard!.instantiateViewController(withIdentifier: "calendarMain") as! calendarViewController
//        self.present(vc, animated: true, completion: nil)
//    }
    @IBAction func calendarButton(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "calendarMain") as! calendarViewController
              self.present(vc, animated: true, completion: nil)
    }
    @IBAction func clockButton(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "csMain") as! communityServiceViewController
        self.present(vc, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
