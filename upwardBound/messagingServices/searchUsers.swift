//
//  searchUsers.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/22/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import Foundation
import Quickblox

class UsersSearchResultsController : UITableViewController {
    
    var users : [QBUUser] = []
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell")
        
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        }
        
        let user = self.users[indexPath.row]
        cell?.textLabel?.text = user.login
        
        return cell!
    }
}
