//
//  CreateChannelCell.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/23/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit

class CreateChannelCell: UITableViewCell {

//    @IBOutlet weak var newChannelNameField: UITextField!
//    @IBOutlet weak var createChannelButton: UIButton!
  
    @IBOutlet weak var newChannelNameField: UITextField!
    @IBOutlet weak var createChannelButton: UIButton!
    
}
