//
//  announceDetailViewController.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/26/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Parse

class announceDetailViewController: UIViewController {

    @IBOutlet weak var date: UILabel!
    var eventID = ""
    @IBOutlet weak var announcmentLabel: UILabel!
    @IBOutlet weak var details: UITextView!
    var passedValue = ""
    var isAdmin = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        eventID = passedValue
        fetchData()
    }
    @IBAction func cancelButton(_ sender: Any) {
        let currentUser = PFUser.current()
        self.isAdmin = (currentUser!["isAdmin"])! as! String
        if (self.isAdmin == "1")
        {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "adminLandingPage") as! adminLandingPageViewController
            self.present(vc, animated: true, completion: nil)
        }
        else {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "landingPage") as! landingPageViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func fetchData(){
        let query = PFQuery(className:"classAnnoucement")
        query.whereKey("announceTitle", equalTo:eventID)
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            if error == nil {
                
                for event:PFObject! in objects! {
                    self.announcmentLabel.text = (event.object(forKey:"announceTitle") as? String)!
                    self.date.text = (event.object(forKey:"Date") as? String)!
                    self.details.text = (event.object(forKey:"announceDetail") as? String)!
                  
                }
                
            }
        })
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
