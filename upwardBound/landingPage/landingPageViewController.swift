//
//  landingPageViewController.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/14/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Parse

class landingPageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    @IBOutlet weak var eventTableView: UITableView!
    @IBOutlet weak var announceTableView: UITableView!
    var announceDataParse:NSMutableArray = NSMutableArray()
    var eventDataParse:NSMutableArray = NSMutableArray()
    var name = ""
     var valueToPass = ""
    var dataParse:NSMutableArray = NSMutableArray()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count:Int?
        if tableView == announceTableView{
            count = self.announceDataParse.count
        }
        else if tableView == eventTableView
        {
            count = self.eventDataParse.count
        }
        
        return count!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == announceTableView{
        let indexPath = tableView.indexPathForSelectedRow;
        _ = tableView.cellForRow(at: indexPath!) as UITableViewCell!;
        let event:PFObject = self.announceDataParse.object(at: indexPath!.row) as! PFObject
        valueToPass = (event.object(forKey: "announceTitle") as? String)!
        self.performSegue(withIdentifier: "announceDetailView", sender: self)
        }
        else if tableView == eventTableView
        {
            let indexPath = tableView.indexPathForSelectedRow;
            _ = tableView.cellForRow(at: indexPath!) as UITableViewCell!;
            let event:PFObject = self.eventDataParse.object(at: indexPath!.row) as! PFObject
            valueToPass = (event.object(forKey: "eventTitle") as? String)!
            self.performSegue(withIdentifier: "eventViewDetail", sender: self)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "announceDetailView") {
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destination as! announceDetailViewController
            // your new view controller should have property that will store passed value
            viewController.passedValue = valueToPass
        }
        if (segue.identifier == "eventViewDetail") {
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destination as! eventDetailViewController
            // your new view controller should have property that will store passed value
            viewController.passedValue = valueToPass
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell?
        
        if tableView == announceTableView{
            cell = tableView.dequeueReusableCell(withIdentifier: "announcmentCell", for:indexPath)
            let cellDataParse:PFObject = self.announceDataParse.object(at: indexPath.row) as! PFObject
            cell?.textLabel?.text = cellDataParse.object(forKey: "announceTitle") as? String
            cell?.detailTextLabel?.text = cellDataParse.object(forKey: "Date") as? String
            
        }
            
        else if tableView == eventTableView{
            cell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for:indexPath)
            let cellDataParse:PFObject = self.eventDataParse.object(at: indexPath.row) as! PFObject
            cell?.textLabel?.text = cellDataParse.object(forKey: "eventTitle") as? String
            cell?.detailTextLabel?.text = cellDataParse.object(forKey: "Date") as? String
            
            
        }
        return cell!
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.dataParse.removeAllObjects()
        self.eventDataParse.removeAllObjects()
        fetchAnnounce()
        fetchEvent()
    }
    func fetchAnnounce(){
        let query = PFQuery(className:"classAnnoucement")
        //query.whereKey("userId", equalTo:name)
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            if error == nil {
                
                for object:PFObject! in objects! {
                    self.announceDataParse.add(object)
                }
                self.announceTableView.reloadData()
            }
        })
    }
    func fetchEvent(){
        let query = PFQuery(className:"classEvent")
        //query.whereKey("userId", equalTo:name)
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            if error == nil {
                
                for object:PFObject! in objects! {
                    self.eventDataParse.add(object)
                }
                self.eventTableView.reloadData()
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let currentUser = PFUser.current()
        name = (currentUser?.username)!
        
    }
    @IBAction func logout(_ sender: Any) {
        PFUser.logOut()
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "loginView") as! loginViewController
                self.present(vc, animated: true, completion: nil)
                
            }
        }
    }
    @IBAction func messagesButton(_ sender: Any) {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "messageMain") as! chatsViewController
        let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
        self.present(navController, animated:true, completion: nil)
    }
    @IBAction func calendarButton(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "calendarMain") as! calendarViewController
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func csButton(_ sender: Any) {
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "csMain") as! communityServiceViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
