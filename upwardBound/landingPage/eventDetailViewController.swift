//
//  eventDetailViewController.swift
//  upwardBound
//
//  Created by Victoria Alvizurez on 11/26/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit
import Parse

class eventDetailViewController: UIViewController {

    @IBOutlet weak var eventname: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    var eventID = ""
    var passedValue = ""
    var fname = ""
    var lname = ""
    @IBOutlet weak var eventDetails: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        eventID = passedValue
        fetchData()
        let currentUser = PFUser.current()
        fname = (currentUser?["firstName"])! as! String
        lname = (currentUser?["lastName"])! as! String
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func fetchData(){
        let query = PFQuery(className:"classEvent")
        query.whereKey("eventTitle", equalTo:eventID)
        
        query.findObjectsInBackground(block: { (objects : [PFObject]?, error: Error?) -> Void in
            if error == nil {
                
                for event:PFObject! in objects! {
                    self.eventname.text = (event.object(forKey:"eventTitle") as? String)!
                    self.eventDate.text = (event.object(forKey:"Date") as? String)!
                    self.eventDetails.text = (event.object(forKey:"eventDetails") as? String)!
                    
                }
                
            }
        })
    }

    
    @IBAction func signUpButton(_ sender: Any) {
        var newStudentSignUp = PFObject(className:"studentEvents")
        newStudentSignUp["eventName"] = eventname.text
        newStudentSignUp["studentfName"] = fname
        newStudentSignUp["studentlName"] = lname
        newStudentSignUp.saveInBackground {
            (success: Bool, error: Error?) in
            if (success) {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "landingPage") as! landingPageViewController
                self.present(vc, animated: true, completion: nil)
            } else {
                let alert = UIAlertView(title: "Error", message: "\(String(describing: error))", delegate: self, cancelButtonTitle: "OK")
                alert.show()
            }
            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
